Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Slixmpp
Source: https://codeberg.org/poezio/slixmpp
Comment: Slixmpp is a friendly fork of SleekXMPP

Files: *
Copyright:
    2010-2013 Nathanael C. Fritz
    2010-2013 Lance Stout <lancestout@gmail.com>
    2016-2023 Florent Le Coz <louiz@louiz.org> <xmpp:louiz@louiz.org?message>
              Mathieu Pasquet <mathieui@mathieui.net> <xmpp:mathieui@mathieui.net?message>
License: Expat
Comment:
    SleekXMPP contributors:
    Brian Beggs
    Dann Martens
    Florent Le Coz
    Kevin Smith
    Remko Tronçon
    Te-jé Rogers
    Thom Nichols
    .
    Slixpp contributors:
    Emmanuel Gil Peyrot (Link mauve) <linkmauve@linkmauve.fr>
    Sam Whited <sam@samwhited.com>
    Dan Sully <daniel@electricalrain.com>
    Gasper Zejn <zejn@kiberpipa.org>`_)
    Krzysztof Kotlenga <pocek@users.sf.net>
    Tsukasa Hiiragi <bakalolka@gmail.com>

Files: debian/*
Copyright: 2016 Tanguy Ortolo <tanguy+debian@ortolo.eu>
License: Expat or BSD-3-clause

Files: slixmpp/thirdparty/orderedset.py
Copyright: 2009 Raymond Hettinger
License: Expat

Files: slixmpp/util/sasl/*
Copyright: 2004-2013 David Alan Cridland
License: Expat

Files: slixmpp/thirdparty/gnupg.py
Copyright: 2008-2012 Vinay Sajip
License: BSD-3-clause

Files: slixmpp/plugins/xep_0009/*
Copyright:
    2011 Nathanael C. Fritz
    2011 Dann Martens
License: Expat

Files:
    slixmpp/plugins/xep_0323/*
    slixmpp/plugins/xep_0325/*
Copyright:
    2013 Sustainable Innovation, Joachim.lindborg@sust.se, bjorn.westrom@consoden.se
License: Expat

Files: slixmpp/plugins/xep_0332/*
Copyright:
    2015 Riptide IO, sangeeth@riptideio.com
License: Expat

Files: slixmpp/plugins/xep_0009/remote.py
Copyright:
    2011 Nathanael C. Fritz
    2011 Dann Martens (TOMOTON)
    2010-2015 Benjamin Peterson
License: Expat

Files:
    slixmpp/stringprep.*
    slixmpp/plugins/xep_0070/*
    slixmpp/plugins/xep_0231/*
    slixmpp/plugins/xep_0333/*
Copyright: 2015-2016 Emmanuel Gil Peyrot
License: Expat

Files: docs/_static/*
Copyright:  2007-2011 The Sphinx team
License: BSD-2-clause

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 * Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
